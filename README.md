# Introduction into CICD using Gitlab
The goal of this is project is to document a simple develop-release to AWS flow.

## Requirements
1. Create a node docker build job using .gitlab-ci.yml
2. Create a test job using .gitlab-ci.yml
3. Use Gitflow Workflow for versioning for releases
4. Release...

## 1. Docker Node Image in GitLab CI

## 2. Configuring Jest Tests in GitLab CI
See: [Github Gist](https://gist.github.com/rishitells/3c4536131819cff4eba2c8ab5bbb4570)

## 3. Git-flow Workflow
See: [git-flow cheatsheet by Daniel Kummer](https://danielkummer.github.io/git-flow-cheatsheet/#setup). 