import app from "../src/server.js"
import supertest from "supertest"
const request = supertest(app)

describe('asdad', function () {
  it('gets the test endpoint', async () => {
    const response = await request.get('/test')

    expect(response.status).toBe(200)
    expect(response.body.message).toBe('pass!')
  })
});
